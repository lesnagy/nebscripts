
import textwrap

class MerrillScriptGenerator:
    def __init__(self):
        self._generateModelScript = None
        self._initialPathScript   = None
        
    def generateModelScript(self, 
            mesh, maxEnergyEvals, material, temperature, energyLog, 
            Hmag, Hx, Hy, Hz, output):
        return textwrap.dedent(
            '''
            Set MaxMeshNumber 1

            ReadMesh 1 {mesh}

            Set MaxEnergyEvaluations {maxEnergyEvals}

            ConjugateGradient
            Set ExchangeCalculator 1

            Randomize All Moments
            {material} {temperature} C
            EnergyLog {energyLog}
            External Field Strength {Hmag} mT
            External Field Direction {Hx} {Hy} {Hz}

            Minimize
            WriteMagnetization {output}
            CloseLogfile

            End
            ''').format(mesh=mesh, maxEnergyEvals=maxEnergyEvals, 
                    material=material, temperature=temperature, 
                    energyLog=energyLog, Hmag=Hmag, Hx=Hx, Hy=Hy, Hz=Hz, 
                    output=output)

    def generateModelScriptFile(self, absScriptFile,
            mesh, maxEnergyEvals, material, temperature, energyLog, 
            Hmag, Hx, Hy, Hz, output):
        with open(absScriptFile, 'w') as fout:
            fout.write(self.generateModelScript(mesh, maxEnergyEvals, material, 
                temperature, energyLog, Hmag, Hx, Hy, Hz, output))

    def generateNEBInitialPathScript(self,
            mesh, maxEnergyEvals, maxPathEvals, material, temperature, 
            energyLog, Hmag, Hx, Hy, Hz, magPathStart, magPathEnd, nPath, 
            output):
        return textwrap.dedent(
            '''
            set MaxMeshNumber 1 
            ReadMesh 1 {mesh}

            set MaxEnergyEvaluations {maxEnergyEvals}
            set MaxPathEvaluations {maxPathEvals}

            {material} {temperature} C
            external field strength {Hmag} mT
            external field direction {Hx} {Hy} {Hz}

            Set PathN 2
            ReadMagnetization {magPathStart}
            MagnetizationToPath 1

            ReadMagnetization {magPathEnd}
            MagnetizationToPath 2

            EnergyLog {energyLog}

            RefinePathTo {nPath}

            ConjugateGradient

            Set ExchangeCalculator 1

            MakeInitialPath
            WriteTecPlotPath {output}

            End
            ''').format(mesh=mesh, maxEnergyEvals=maxEnergyEvals, 
                    maxPathEvals=maxPathEvals, material=material, 
                    temperature=temperature, energyLog=energyLog, Hmag=Hmag,
                    Hx=Hx, Hy=Hy, Hz=Hz, magPathStart=magPathStart, 
                    magPathEnd=magPathEnd, nPath=nPath, output=output)

    def generateNEBInitialPathScriptFile(self, absScriptFile,
            mesh, maxEnergyEvals, maxPathEvals, material, temperature, 
            energyLog, Hmag, Hx, Hy, Hz, magPathStart, magPathEnd, nPath, 
            output):
        with open(absScriptFile, 'w') as fout:
            fout.write(self.generateNEBInitialPathScript(mesh, maxEnergyEvals, 
                maxPathEvals, material, temperature, energyLog, Hmag, Hx, Hy, 
                Hz, magPathStart, magPathEnd, nPath, output))

    def generateNEBMinimizedPathScript(self, maxEnergyEvals, material,
            temperature, Hmag, Hx, Hy, Hz, initialPath, energyLog, output):
        return textwrap.dedent(
            '''
            ReadTecplotPath {initialPath}

            Set MaxEnergyEvaluations {maxEnergyEvals}

            {material} {temperature} C
            external field strength {Hmag} mT
            external field direction {Hx} {Hy} {Hz}

            EnergyLog {energyLog}

            ConjugateGradient

            Set ExchangeCalculator 1

            PathMinimize

            CloseLogFile
            WriteTecPlotPath {output}

            End
            ''').format(maxEnergyEvals=maxEnergyEvals, 
                    material=material, temperature=temperature, Hmag=Hmag,
                    Hx=Hx, Hy=Hy, Hz=Hz, initialPath=initialPath, 
                    energyLog=energyLog, output=output)

    def generateNEBMinimizedPathScriptFile(self, absScriptFile,
            maxEnergyEvals, material, temperature, Hmag, Hx, Hy, Hz, 
            initialPath, energyLog, output):
        with open(absScriptFile, 'w') as fout:
            fout.write(self.generateNEBMinimizedPathScript( 
                maxEnergyEvals, material, temperature, Hmag, Hx, Hy, Hz, 
                initialPath, energyLog, output))

    def generatEnergyPathScript(self, pathFile, material, temperature,
            Hmag, Hx, Hy, Hz, energyPathFile):
        return textwrap.dedent(
            '''
            ReadTecPlotPath {pathFile}
            {material} {temperature} C
            External Field Strength {Hmag} mT
            External Field Direction {Hx} {Hy} {Hz}


            PathStructureEnergies {energyPathFile}
            ''').format(pathFile=pathFile, material=material, 
                    temperature=temperature, Hmag=Hmag, Hx=Hx, Hy=Hy, Hz=Hz,
                    energyPathFile=energyPathFile)

    def generatEnergyPathScriptFile(self, absScriptFile, 
            pathFile, material, temperature, Hmag, Hx, Hy, Hz, energyPathFile):
        with open(absScriptFile, 'w') as fout:
            fout.write(self.generatEnergyPathScript(pathFile, material, 
                temperature, Hmag, Hx, Hy, Hz, energyPathFile))

if __name__ == '__main__':

    msg = MerrillScriptGenerator()
    print("*****************************************************")
    print("Model script")
    print("*****************************************************")

    print msg.generateModelScript('mesh.pat', 
            2000, 'iron', 20, 'energylog', 0.0, -1.0, -1.0, -1.0, 'output')
    
    msg.generateModelScriptFile('_modelscriptfile', 'mesh.pat', 
            2000, 'iron', 20, 'energylog', 0.0, -1.0, -1.0, -1.0, 'output')

    print("*****************************************************")
    print("")

    print("*****************************************************")
    print("NEB initial path script")
    print("*****************************************************")

    print msg.generateNEBInitialPathScript('mesh.pat',
            2000, 4000, 'iron', 20, 'energylog', 0.0, -1.0, -1.0, -1.0, 
            'magStartPath', 'magEndPath', 100, 'myoutputfile')

    msg.generateNEBInitialPathScriptFile('_nebinitpathfile', 'mesh.pat',
            2000, 4000, 'iron', 20, 'energylog', 0.0, -1.0, -1.0, -1.0, 
            'magStartPath', 'magEndPath', 100, 'myoutputfile')

    print("*****************************************************")
    print("")

    print("*****************************************************")
    print("NEB script")
    print("*****************************************************")

    print msg.generateNEBMinimizedPathScript('mesh.pat', 
            2000, 'iron', 20, 0.0, -1.0, -1.0, -1.0, 'initialpathfile', 
            'energylogfile', 'NEBPathFinal.tec')

    msg.generateNEBMinimizedPathScriptFile('_minimizedscript', 'mesh.pat', 
            2000, 'iron', 20, 0.0, -1.0, -1.0, -1.0, 'initialpathfile', 
            'energylogfile', 'NEBPathFinal.tec')

    print("*****************************************************")
    print("")

    print("*****************************************************")
    print("Energy path script")
    print("*****************************************************")

    print msg.generatEnergyPathScript('mypathfile.tec', 'iron', 20, 0.0, 
            -1.0, -1.0, -1.0, 'energypathfile.dat')

    msg.generatEnergyPathScriptFile('_energypath',
            'mypathfile.tec', 'iron', 20, 0.0, -1.0, -1.0, -1.0, 
            'energypathfile.dat')

    print("*****************************************************")
    print("")
