#!/usr/bin/env python

import os, sys

from DirectoryDatabase      import *
from CommandLineParser      import *
from ScriptEnvironment      import *
from MerrillScriptGenerator import *
from Messages               import *

if __name__ == '__main__':
    se  = ScriptEnvironment()
    cmd = CommandLine('material', 'geometry', 'sizes', 'temperatures', 'indices')
    dd  = DirectoryDatabase(se.modelBaseDir())
    msg = MerrillScriptGenerator()

    cmd.parseArgv(sys.argv)

    material = cmd.getArg('material')
    if not dd.isMaterial(material):
        dd.createMaterial(material)

    geometry = cmd.getArg('geometry')
    if not dd.isGeometry(material, geometry):
        dd.createGeometry(material, geometry)

    sizes = cmd.getArg('sizes')
    for size in sizes:
        indexCount = 0
        if not dd.isSize(material, geometry, size):
            dd.createSize(material, geometry, size)
        temps = cmd.getArg('temperatures')

        absMeshFile = os.path.join(dd.meshFilesDir(material, geometry), dd.meshFileName(size))
        if not os.path.isfile(absMeshFile):
            warning("Mesh '{}' not found".format(absMeshFile))

        for temp in temps:
            abstemp = dd.createTemperature(material, geometry, size, temp)
            indices = cmd.getArg('indices')
            for index in indices:
                if not dd.isLemScriptFile(material, geometry, size, temp, index):
                    # Create the file
                    absLEMFile    = os.path.join(abstemp, dd.lemScriptFileName(size, temp, index))
                    absEnergyFile = os.path.join(abstemp, dd.energyFileStubName(size, temp, index))
                    absOutputFile = os.path.join(abstemp, dd.outputMagFileName(size, temp, index))

                    msg.generateModelScriptFile(absLEMFile, 
                            absMeshFile, 10000, material, temp, absEnergyFile,
                            0.0, -1.0, -1.0, -1.0, absOutputFile)

                    #info("Created file '{}'".format(absLEMFile))
                    indexCount = indexCount + 1
                else:
                    absLEMFile    = os.path.join(abstemp, dd.lemScriptFileName(size, temp, index))
                    warning("LEM script '{}' already exists.".format(absLEMFile))
        info("Created {} scripts for {}, {}, {}".format(indexCount, 
            material, geometry, '{}nm'.format(size), ))

