import os

class ScriptEnvironment:
    def __init__(self):
        self._modelBaseDir = os.environ.get('NEB_MODEL_BASE_DIR',              
                                           '/models/NEB_DATABASE_TEST')

    def modelBaseDir(self):
        return self._modelBaseDir

