import os, sys, re

from ScriptEnvironment import *

class DirectoryDatabase:
    def __init__(self, name):
        self._name = name
        self._regexName = re.compile(r'^{}[/]?'.format(name))
        
        self._regexStdOut        = re.compile(r'stdout_lcl_([0-9]+)nm_([0-9]+)C_([0-9]{4})')
        self._regexStdErr        = re.compile(r'stderr_lcl_([0-9]+)nm_([0-9]+)C_([0-9]{4})')
        self._regexRemoteScript  = re.compile(r'rmt_([0-9]+)nm_([0-9]+)C_([0-9]{4})')
        self._regexLocalScript   = re.compile(r'lcl_([0-9]+)nm_([0-9]+)C_([0-9]{4})')
        self._regexDatFile       = re.compile(r'([0-9]+)nm_([0-9]+)C_mag_([0-9]{4})\.dat')
        self._regexTecFile       = re.compile(r'([0-9]+)nm_([0-9]+)C_mag_([0-9]{4})_mult\.tec')
        self._regexEnergyFile    = re.compile(r'([0-9]+)nm_([0-9]+)C_energy_([0-9]{4})\.log')
        self._regexStartEndFile  = re.compile(r'START_END(_([0-9]{4}))?')
        self._regexMeshFile      = re.compile(r'([0-9]+)\.pat')
        self._regexSize          = re.compile(r'([0-9]+)nm')
        self._regexTemperature   = re.compile(r'([0-9]+)C')

        self._regexIPathScriptFile = re.compile(r'nebinitial(_start_end_([0-9]{4}))?')
        self._regexIPathEnergyFile = re.compile(r'nebinitial_energy(_start_end_([0-9]{4}))?')
        self._regexIPathTecFile    = re.compile(r'initialpath(_start_end(_([0-9]{4})?)?)?\.tec')
        self._regexIPathStdOut     = re.compile(r'stdout_nebinitial(_start_end_([0-9]{4}))?')
        self._regexIPathStdErr     = re.compile(r'stderr_nebinitial(_start_end_([0-9]{4}))?')

        self._regexFPathScriptFile = re.compile(r'nebfinal(_start_end_([0-9]{4}))?')
        self._regexFPathEnergyFile = re.compile(r'nebfinal_energy(_start_end_([0-9]{4}))?')
        self._regexFPathTecFile    = re.compile(r'finalpath(_start_end(_([0-9]{4})?)?)?\.tec')
        self._regexFPathStdOut     = re.compile(r'stdout_nebfinal(_start_end_([0-9]{4}))?')
        self._regexFPathStdErr     = re.compile(r'stderr_nebfinal(_start_end_([0-9]{4}))?')

        self._formatStdOut       = 'stdout_lcl_{:d}nm_{:d}C_{:04d}'
        self._formatStdErr       = 'stderr_lcl_{:d}nm_{:d}C_{:04d}'
        self._formatRemoteScript = 'rmt_{:d}nm_{:d}C_{:04d}'
        self._formatLocalScript  = 'lcl_{:d}nm_{:d}C_{:04d}'
        self._formatDatFile      = '{:d}nm_{:d}C_mag_{:04d}.dat'
        self._formatTecFile      = '{:d}nm_{:d}C_mag_{:04d}_mult.tec'
        self._formatEnergyFile   = '{:d}nm_{:d}C_energy_{:04d}.log'
        self._formatMeshFile     = '{:d}.pat'
        self._formatOutputMag    = '{:d}nm_{:d}C_mag_{:04d}'

        self._formatIPathScriptFile = 'nebinitial_{}'
        self._formatIPathEnergyFile = 'nebinitial_energy_{}'
        self._formatIPathTecFile    = 'initialpath_{}.tec'
        self._formatIPathStdOut     = 'stdout_nebinitial_{}'
        self._formatIPathStdErr     = 'stderr_nebinitial_{}'

        self._formatFPathScriptFile = 'nebfinal_{}'
        self._formatFPathEnergyFile = 'nebfinal_energy_{}'
        self._formatFPathTecFile    = 'finalpath_{}.tec'
        self._formatFPathStdOut     = 'stdout_nebfinal_{}'
        self._formatFPathStdErr     = 'stderr_nebfinal_{}'

    # Break an absolute path in the form of 
    #  DATABASE_NAME/material/geometry/lems/size/temperature/file 
    # in to its component parts.
    def absPathFileToComponents(self, abspath):
        components = ['file', 'temperature', 'size', 'lems', 'geometry', 'material']

        # Get rid of the root directory from the abspath
        truncAbspath = re.sub(self._regexName, '', abspath)

        # Split up the truncated abspath
        split = truncAbspath.split('/')

        # There should be exactly the correct number of components in the path
        if len(split) != len(components):
            raise ValueError("The path does not contain enough components")
        
        result = {}
        pitems = [self._name]
        for item in split:
            currentComponent = components.pop()
            pitems.append(item)
            checkpath = os.path.join(self._name, '/'.join(pitems))

            # Sanity check.
            if currentComponent != 'file':
                # Check that the directory formed from the components exists
                # Unless it is a file component, then it is possible that the 
                # file component does not exist.
                if not (os.path.isfile(checkpath) or os.path.isdir(checkpath)):
                    raise ValueError("SANITY ERROR!! - The path does not conform!")

            if currentComponent == 'lems':
                # ignore processing of the 'lems' component (i.e. don't include
                # in the output).
                continue
            elif currentComponent == 'temperature':
                result[currentComponent] = self.tempAsNumber(item)
            elif currentComponent == 'size':
                result[currentComponent] = self.sizeAsNumber(item)
            else:
                result[currentComponent] = item

        return result

    def initialPathScriptAbsPath(self, material, geometry, size, temp, suffix):
        abspath = os.path.join(self._name, material, geometry, 'lems', 
                self.sizeAsString(size), self.tempAsString(temp), 
                self._formatIPathScriptFile.format(suffix))
        return {'abs': abspath, 'exists': os.path.isfile(abspath)}

    def initialPathEnergyFileAbsPath(self, material, geometry, size, temp, suffix):
        abspath = os.path.join(self._name, material, geometry, 'lems', 
                self.sizeAsString(size), self.tempAsString(temp), 
                self._formatIPathEnergyFile.format(suffix))
        return {'abs': abspath, 'exists': os.path.isfile(abspath)}

    def initialPathDataFileAbsPath(self, material, geometry, size, temp, suffix):
        abspath = os.path.join(self._name, material, geometry, 'lems', 
                self.sizeAsString(size), self.tempAsString(temp), 
                self._formatIPathTecFile.format(suffix))
        return {'abs': abspath, 'exists': os.path.isfile(abspath)}

    def finalPathScriptAbsPath(self, material, geometry, size, temp, suffix):
        abspath = os.path.join(self._name, material, geometry, 'lems', 
                self.sizeAsString(size), self.tempAsString(temp), 
                self._formatFPathScriptFile.format(suffix))
        return {'abs': abspath, 'exists': os.path.isfile(abspath)}

    def finalPathEnergyFileAbsPath(self, material, geometry, size, temp, suffix):
        abspath = os.path.join(self._name, material, geometry, 'lems', 
                self.sizeAsString(size), self.tempAsString(temp), 
                self._formatFPathEnergyFile.format(suffix))
        return {'abs': abspath, 'exists': os.path.isfile(abspath)}

    def finalPathDataFileAbsPath(self, material, geometry, size, temp, suffix):
        abspath = os.path.join(self._name, material, geometry, 'lems', 
                self.sizeAsString(size), self.tempAsString(temp), 
                self._formatFPathTecFile.format(suffix))
        return {'abs': abspath, 'exists': os.path.isfile(abspath)}

    def iPathStdOut(self, suffix):
        return self._formatIPathStdOut.format(suffix)

    def iPathStdErr(self, suffix):
        return self._formatIPathStdErr.format(suffix)

    #### Material

    def materials(self, fullPath=False):
        materials = []
        for m in os.listdir(self._name):
            mabs = os.path.join(self._name, m)
            if os.path.isdir(mabs):
                if fullPath:
                    materials.append(os.path.join(self._name, m))
                else:
                    materials.append(m)

        return materials

    def isMaterial(self, name):
        mats = self.materials()
        if name in mats:
            return True
        return False

    def createMaterial(self, name):
        os.mkdir(os.path.join(self._name, name))

    def materialAbsPath(self, name):
        absdir = os.path.join(os.path.join(self._name, name))
        return {'abs': absdir, 'exists': os.path.isdir(absdir) }
        

    #### Geometry

    def geometries(self, material, fullPath=False):
        geometries = []
        mabs = os.path.join(self._name, material)
        for g in os.listdir(mabs):
            gabs = os.path.join(mabs, g)
            if os.path.isdir(gabs):
                if fullPath:
                    geometries.append(os.path.join(mabs, g))
                else:
                    geometries.append(g)

        return geometries

    def isGeometry(self, material, name):
        geoms = self.geometries(material)
        if name in geoms:
            return True
        return False

    def createGeometry(self, material, name):
        if not self.isMaterial(material):
            raise ValueError("Attempting to create geometry '{}' for unknown material '{}'".format(
                name, material))

        mabs = os.path.join(self._name, material)

        os.mkdir(os.path.join(mabs, name))
        os.mkdir(os.path.join(mabs, name, 'lems'))
        os.mkdir(os.path.join(mabs, name, 'meshes'))

    def geometryAbsPath(self, material, geometry):
        absdir = os.path.join(self._name, material, geometry)
        return {'abs': absdir, 'exists': os.path.isdir(absdir)}

    #### Size

    def sizes(self, material, geometry, fullPath=False):
        sizes = []
        mabs = os.path.join(self._name, material)
        gabs = os.path.join(mabs, geometry)
        for s in os.listdir(os.path.join(gabs, 'lems')):
            sabs = os.path.join(gabs, 'lems', s)
            if os.path.isdir(sabs):
                if fullPath:
                    sizes.append(os.path.join(gabs, 'lems', s))
                else:
                    sizes.append(s)

        return sizes

    def isSize(self, material, geometry, size):
        sizes = self.sizes(material, geometry)
        if self.sizeAsString(size) in sizes:
            return True
        return False

    def createSize(self, material, geometry, size):
        if not self.isMaterial(material):
            raise ValueError("Unknown material '{}'".format(material))
        if not self.isGeometry(material, geometry):
            raise ValueError("Unknown geometry '{}'".format(geometry))

        mabs = os.path.join(self._name, material)
        gabs = os.path.join(mabs, geometry)

        newSizeDir = os.path.join(gabs, 'lems', self.sizeAsString(size))
        if os.path.isdir(newSizeDir):
            return newSizeDir
        else:
            os.mkdir(newSizeDir)
            return newSizeDir

    def sizeAbsPath(self, material, geometry, size):
        absdir = os.path.join(self._name, material, geometry, 'lems', self.sizeAsString(size))
        return {'abs': absdir, 'exists': os.path.isdir(abspath)}

    #### Temperature

    def temperatures(self, material, geometry, size, fullPath=False):
        temperatures = []
        mabs = os.path.join(self._name, material)
        gabs = os.path.join(mabs, geometry)
        sabs = os.path.join(gabs, 'lems', self.sizeAsString(size))
        for t in os.listdir(sabs):
            tabs = os.path.join(sabs, t)
            if os.path.isdir(tabs):
                if fullPath:
                    temperatures.append(os.path.join(sabs, t))
                else:
                    temperatures.append(t)

        return temperatures

    def isTemperature(self, material, geometry, size, temp):
        temps = self.temperatures(material, geometry, self.sizeAsString(size))
        if self.tempAsString(temp) in temps:
            return True
        return False

    def createTemperature(self, material, geometry, size, temp):
        if not self.isMaterial(material):
            raise ValueError("Unknown material '{}'".format(material))
        if not self.isGeometry(material, geometry):
            raise ValueError("Unknown geometry '{}'".format(geometry))
        if not self.isSize(material, geometry, self.sizeAsString(size)):
            raise ValueError("Unknown size '{}'".format(size))

        mabs = os.path.join(self._name, material)
        gabs = os.path.join(mabs, geometry)
        sabs = os.path.join(gabs, 'lems', self.sizeAsString(size))

        newTempDir = os.path.join(sabs, self.tempAsString(temp))
        if os.path.isdir(newTempDir):
            return newTempDir
        else:
            os.mkdir(newTempDir)
            return newTempDir

    def temperatureAbsPath(self, material, geometry, size, temperature):
        absdir = os.path.join(self._name, material, geometry, 'lems', self.sizeAsString(size), 
                self.tempAsString(temperature))
        return {'abs': absdir, 'exists': os.path.isdir(absdir)}

    #### LEM scripts

    def lemScriptFileName(self, size, temperature, index):
        return self._formatLocalScript.format(size, temperature, index)

    def lemScriptFiles(self, material, geometry, size, temperature, fullPath=False):
        lemScriptFiles = []
        mabs = os.path.join(self._name, material)
        gabs = os.path.join(mabs, geometry)
        sabs = os.path.join(gabs, 'lems', self.sizeAsString(size))
        tabs = os.path.join(sabs, self.tempAsString(temperature))

        for f in os.listdir(tabs):
            matchLemScriptFile = self._regexLocalScript.match(f)
            if matchLemScriptFile:
                if fullPath:
                    lemScriptFiles.append(os.path.join(tabs, f))
                else:
                    lemScriptFiles.append(f)

        return lemScriptFiles

    def isLemScriptFile(self, material, geometry, size, temperature, index):
        lemScriptFiles = self.lemScriptFiles(material, geometry, self.sizeAsString(size), self.tempAsString(temperature))
        if self.lemScriptFileName(size, temperature,index) in lemScriptFiles:
            return True
        return False

    def lemScriptFileAbsPath(self, material, geometry, size, temperature, index):
        abspath = os.path.join(self._name, material, geometry, 'lems', 
                self.sizeAsString(size), self.tempAsString(temperature),
                self._formatLocalScript.format(size, temperature, index))
        return {'abs': abspath, 'exists': os.path.isfile(abspath)}

    #### stdout file

    def stdOutFiles(self, material, geometry, size, temperature, fullPath=False):
        stdOutFiles = []
        mabs = os.path.join(self._name, material)
        gabs = os.path.join(mabs, geometry)
        sabs = os.path.join(gabs, 'lems', self.sizeAsString(size))
        tabs = os.path.join(sabs, self.tempAsString(temperature))

        for f in os.listdir(tabs):
            matchStdOut = self._regexStdOut.match(f)
            if matchStdOut:
                if fullPath:
                    stdOutFiles.append(os.path.join(tabs, f))
                else:
                    stdOutFiles.append(f)

        return stdOutFiles

    def stdOutFileAbsPath(self, material, geometry, size, temperature, index):
        abspath = os.path.join(self._name, material, geometry, 'lems', 
                self.sizeAsString(size), self.tempAsString(temperature),
                self._formatStdOut.format(size, temperature, index))
        return {'abs': abspath, 'exists': os.path.isfile(abspath)}

    #### stderr file

    def stdErrFiles(self, material, geometry, size, temperature, fullPath=False):
        stdErrFiles = []
        mabs = os.path.join(self._name, material)
        gabs = os.path.join(mabs, geometry)
        sabs = os.path.join(gabs, 'lems', self.sizeAsString(size))
        tabs = os.path.join(sabs, self.tempAsString(temperature))

        for f in os.listdir(tabs):
            matchStdErr = self._regexStdErr.match(f)
            if matchStdErr:
                if fullPath:
                    stdErrFiles.append(os.path.join(tabs, f))
                else:
                    stdErrFiles.append(f)

        return stdErrFiles

    def stdErrAbsPath(self, material, geometry, size, temperature, index):
        abspath = os.path.join(self._name, material, geometry, 'lems', 
                self.sizeAsString(size), self.tempAsString(temperature),
                self._formatStdErr.format(size, temperature, index))
        return {'abs': abspath, 'exists': os.path.isfile(abspath)}

    #### dat file

    def datFiles(self, material, geometry, size, temperature, fullPath=False):
        datFiles = []
        mabs = os.path.join(self._name, material)
        gabs = os.path.join(mabs, geometry)
        sabs = os.path.join(gabs, 'lems', self.sizeAsString(size))
        tabs = os.path.join(sabs, self.tempAsString(temperature))

        for f in os.listdir(tabs):
            matchDatFile = self._regexDatFile.match(f)
            if matchDatFile:
                if fullPath:
                    datFiles.append(os.path.join(tabs, f))
                else:
                    datFiles.append(f)

        return datFiles

    def datFileAbsPath(self, material, geometry, size, temperature, index):
        abspath = os.path.join(self._name, material, geometry, 'lems', 
                self.sizeAsString(size), self.tempAsString(temperature),
                self._formatDatFile.format(size, temperature, index))
        return {'abs': abspath, 'exists': os.path.isfile(abspath)}

    def datFileToTecplotFile(self, absFileName):
        fnsplit = absFileName.split("/")

        matchDatFile = self._regexDatFile.search(fnsplit[-1])
        if matchDatFile:

            tecFile = self._formatTecFile.format(
                    int(matchDatFile.group(1)),
                    int(matchDatFile.group(2)),
                    int(matchDatFile.group(3)))

            return os.path.join(
                    "/".join(fnsplit[0:-1]), tecFile)

    #### tec file
    
    def tecFileName(self, size, temperature, index):
        return self._formatTecFile.format(size, temperature, index)

    def tecFiles(self, material, geometry, size, temperature, fullPath=False):
        tecFiles = []
        mabs = os.path.join(self._name, material)
        gabs = os.path.join(mabs, geometry)
        sabs = os.path.join(gabs, 'lems', self.sizeAsString(size))
        tabs = os.path.join(sabs, self.tempAsString(temperature))

        for f in os.listdir(tabs):
            matchTecFile = self._regexTecFile.match(f)
            if matchTecFile:
                if fullPath:
                    tecFiles.append(os.path.join(tabs, f))
                else:
                    tecFiles.append(f)

        return tecFiles

    def isTecFile(self, material, geometry, size, temperature, index):
        tecFiles = self.tecFiles(self, material, geometry, size, temperature)
        if self.tecFileName(size, temperature, index) in tecFiles:
            return True
        return False

    def tecFileAbsPath(self, material, geometry, size, temperature, index):
        abspath = os.path.join(self._name, material, geometry, 'lems', 
                self.sizeAsString(size), self.tempAsString(temperature),
                self._formatTecFile.format(size, temperature, index))
        return {'abs': abspath, 'exists': os.path.isfile(abspath)}

    #### initial path tecplot files

    def initTecFileName(self, suffix):
        return self._formatIPathTecFile.format(suffix)

    def initTecFiles(self, material, geometry, size, temperature, fullPath=False):
        tecFiles = []
        mabs = os.path.join(self._name, material)
        gabs = os.path.join(mabs, geometry)
        sabs = os.path.join(gabs, 'lems', self.sizeAsString(size))
        tabs = os.path.join(sabs, self.tempAsString(temperature))

        for f in os.listdir(tabs):
            matchTecFile = self._regexIPathTecFile.match(f)
            if matchTecFile:
                if fullPath:
                    tecFiles.append(os.path.join(tabs, f))
                else:
                    tecFiles.append(f)

        return tecFiles

    def isInitTecFile(self, material, geometry, size, temperature, suffix):
        tecFiles = self.initTecFiles(material, geometry, size, temperature)
        # print("tecFiles: {}".format(tecFiles))
        if self.initTecFileName(suffix) in tecFiles:
            return True
        return False

    def initTecFileAbsPath(self, material, geometry, size, temperature, suffix):
        abspath = os.path.join(self._name, material, geometry, 'lems', 
                self.sizeAsString(size), self.tempAsString(temperature),
                self.initTecFileName(suffix))
        return {'abs': abspath, 'exists': os.path.isfile(abspath)}

    #### final path tecplot files

    def finalTecFileName(self, suffix):
        return self._formatFPathTecFile.format(suffix)

    def finalTecFiles(self, material, geometry, size, temperature, fullPath=False):
        tecFiles = []
        mabs = os.path.join(self._name, material)
        gabs = os.path.join(mabs, geometry)
        sabs = os.path.join(gabs, 'lems', self.sizeAsString(size))
        tabs = os.path.join(sabs, self.tempAsString(temperature))

        for f in os.listdir(tabs):
            matchTecFile = self._regexFPathTecFile.match(f)
            if matchTecFile:
                if fullPath:
                    tecFiles.append(os.path.join(tabs, f))
                else:
                    tecFiles.append(f)

        return tecFiles

    def isFinalTecFile(self, material, geometry, size, temperature, suffix):
        tecFiles = self.finalTecFiles(material, geometry, size, temperature)
        # print("tecFiles: {}".format(tecFiles))
        if self.finalTecFileName(suffix) in tecFiles:
            return True
        return False

    def finalTecFileAbsPath(self, material, geometry, size, temperature):
        abspath = os.path.join(self._name, material, geometry, 'lems', 
                self.sizeAsString(size), self.tempAsString(temperature),
                self.finalTecFileName())
        return {'abs': abspath, 'exists': os.path.isfile(abspath)}

    #### energy file

    def energyFiles(self, material, geometry, size, temperature, fullPath=False):
        energyFiles = []
        mabs = os.path.join(self._name, material)
        gabs = os.path.join(mabs, geometry)
        sabs = os.path.join(gabs, 'lems', self.sizeAsString(size))
        tabs = os.path.join(sabs, self.tempAsString(temperature))

        for f in os.listdir(tabs):
            matchEnergyFile = self._regexEnergyFile.match(f)
            if matchEnergyFile:
                if fullPath:
                    energyFiles.append(os.path.join(tabs, f))
                else:
                    energyFiles.append(f)

        return energyFiles

    def energyFileName(self, size, temp, index):
        return self._formatEnergyFile.format(size, temp, index)

    def energyFileAbsPath(self, material, geometry, size, temperature, index):
        abspath = os.path.join(self._name, material, geometry, 'lems', 
                self.sizeAsString(size), self.tempAsString(temperature),
                self._formatEnergyFile.format(size, temperature, index))
        return {'abs': abspath, 'exists': os.path.isfile(abspath)}

    #### output mag files
    def outputMagFileName(self, size, temp, index):
        return self._formatOutputMag.format(size, temp, index)

    #### start/end files

    def hasStartEndFiles(self, material, geometry, size, temperature):
        # Make sure we have at least one start/end file
        mabs = os.path.join(self._name, material)
        gabs = os.path.join(mabs, geometry)
        sabs = os.path.join(gabs, 'lems', self.sizeAsString(size))
        tabs = os.path.join(sabs, self.tempAsString(temperature))

        for f in os.listdir(tabs):
            matchStartEnd = self._regexStartEndFile.match(f)
            if matchStartEnd:
                return True

        return False

    def startEndPairs(self, material, geometry, size, temperature, fullPath=False):
        if not self.hasStartEndFiles(material, geometry, size, temperature):
            return []

        mabs = os.path.join(self._name, material)
        gabs = os.path.join(mabs, geometry)
        sabs = os.path.join(gabs, 'lems', self.sizeAsString(size))
        tabs = os.path.join(sabs, self.tempAsString(temperature))

        output = []
        for f in os.listdir(tabs):
            matchStartEnd = self._regexStartEndFile.match(f)
            if matchStartEnd:
                startEnd = []
                with open(os.path.join(tabs, matchStartEnd.group(0)), 'r') as fin:
                    for line in fin:
                        startEnd.append(line.strip())
                if len(startEnd) == 2:
                    matchStart = self._regexDatFile.match(startEnd[0])
                    matchEnd   = self._regexDatFile.match(startEnd[1])
                    if matchStart and matchEnd:
                        if fullPath:
                            output.append({'name' : matchStartEnd.group(0),
                                           'start': os.path.join(tabs, startEnd[0]), 
                                           'end'  : os.path.join(tabs, startEnd[1])})
                        else:
                            output.append({'name' : matchStartEnd.group(0),
                                           'start': startEnd[0], 
                                           'end'  : startEnd[1]})

        return output

    #### mesh files

    def meshFiles(self, material, geometry, fullPath=False):
        meshfiles = []

        mabs   = os.path.join(self._name, material)
        gabs   = os.path.join(mabs, geometry)
        mshabs = os.path.join(gabs, 'meshes')

        for f in os.listdir(mshabs):
            matchMeshFile = self._regexMeshFile.match(f)
            if matchMeshFile:
                if fullPath:
                    meshfiles.append(os.path.join(gabs, f))
                else:
                    meshfiles.append(f)

    def isMeshFile(self, material, geometry, size):
        meshfiles = self.meshFiles(material, geometry)

        meshFileName = self._formatMeshFile.format(size)
        if meshFileName in meshfiles:
            return true

        return false

    def meshFileName(self, size):
        return self._formatMeshFile.format(size)

    def meshFileAbsPath(self, material, geometry, size):
        abspath = os.path.join(self._name, material, geometry, 'meshes', 
                self._formatMeshFile.format(size))
        return {'abs': abspath, 'exists': os.path.join(abspath)}

    def meshFilesDir(self, material, geometry):
        mabs   = os.path.join(self._name, material)
        gabs   = os.path.join(mabs, geometry)
        mshabs = os.path.join(gabs, 'meshes')

        if os.path.isdir(mshabs):
            return mshabs
        return None

    #### Size/temperature string/numeric conversion functions

    def sizeAsNumber(self, size):
        if isinstance(size, str):
            matchSize = self._regexSize.match(size)
            if not matchSize:
                raise ValueError("Size not in valid format.")
            else:
                return int(matchSize.group(1))
        elif isinstance(size, int):
            return size
        elif isinstance(size, float):
            return size
        else:
            raise ValueError("Size not valid object.")

    def sizeAsString(self, size):
        if isinstance(size, str):
            if not self._regexSize.match(size):
                raise ValueError("Size in not in valid format.")
            else:
                return size
        elif isinstance(size, int):
            return '{}nm'.format(size)
        elif isinstance(size, float):
            return'{}nm'.format(size)
        else:
            raise ValueError("Size not valid object.")

    def tempAsNumber(self, temp):
        if isinstance(temp, str):
            matchTemperature = self._regexTemperature.match(temp)
            if not matchTemperature:
                raise ValueError("Temperature not in valid format.")
            else:
                return int(matchTemperature.group(1))
        elif isinstance(temp, int):
            return temp
        elif isinstance(temp, float):
            return temp
        else:
            raise ValueError("Temperature not valid object.")

    def tempAsString(self, temp):
        if isinstance(temp, str):
            if not self._regexTemperature.match(temp):
                raise ValueError("Temperature in not in valid format.")
            else:
                return temp
        elif isinstance(temp, int):
            return '{}C'.format(temp)
        elif isinstance(temp, float):
            return'{}C'.format(temp)
        else:
            raise ValueError("Temperature not valid object.")



if __name__ == '__main__':
    senv = ScriptEnvironment()
    dd = DirectoryDatabase(senv.modelBaseDir())

    fp = True

    for m in dd.materials(fullPath=fp):
        print(m)
        for g in dd.geometries(m, fullPath=fp):
            print("\t{}".format(g))
            for s in dd.sizes(m, g, fullPath=fp):
                print("\t\t{}".format(s))
                for t in dd.temperatures(m, g, s, fullPath=fp):
                    if dd.hasStartEndFiles(m, g, s, t):
                        startEnd = dd.startEnd(m, g, s, t, fullPath=fp)
                        print("\t\t\t{} - {} {}".format(t, startEnd['start'], startEnd['end']))
                    else:
                        print("\t\t\t{}".format(t))

    print("")

    for sof in dd.stdOutFiles('iron', 'spheres', '40nm', '40C'):
        print sof

    print("")

    for sef in dd.stdErrFiles('iron', 'spheres', '40nm', '40C'):
        print sef

    print("")

    for df in dd.datFiles('iron', 'spheres', '40nm', '40C'):
        print df
        
    print("")

    for tf in dd.tecFiles('iron', 'spheres', '40nm', '40C'):
        print tf

    print("")

    for ef in dd.energyFiles('iron', 'spheres', '40nm', '40C'):
        print ef

    dd.createMaterial('kryptonite')
    dd.createGeometry('adamanite', 'tesseract')
