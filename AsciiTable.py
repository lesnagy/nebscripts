class AsciiTable:

  def __init__(self, cols):
    self._nrows        = 0
    self._ncols        = cols
    self._headings     = ['']*self._ncols
    self._data         = []
    self._barBelow     = []
    self._barAbove     = []
    self._maxColWidths = [0]*self._ncols

    self._defaultBlank       = 'n/a'
    self._defaultFloatFormat = '{:020.15f}'
    self._defaultIntFormat   = '{:d}'

    self._joint = '+'
    self._bar   = '|'
    self._horiz = '-'

    self._nohead = False

  def __str__(self):

    # A table frame.
    lstFrameBar = []
    lstRowSeparator = []
    lstDataRow = []
    for i in range(len(self._maxColWidths)):
      w = self._maxColWidths[i]
      lstFrameBar.append(self._horiz*(w+2))
      lstRowSeparator.append(self._horiz*(w+2))
      lstDataRow.append(' {{{}:<{}}} '.format(i,w))
    frameBar = self._joint + self._joint.join(lstFrameBar) + self._joint
    rowSeparator = self._bar + self._bar.join(lstRowSeparator) + self._bar
    dataRow = self._bar + self._bar.join(lstDataRow) + self._bar

    # Create string version of the table.
    strTable  = ''
    strTable += frameBar + '\n'
    if not self._nohead:
      strTable += dataRow.format(*self._headings) + '\n'
      strTable += frameBar + '\n'

    for i in range(len(self._data)):
      item = self._data[i]
      if self._barAbove[i]:
        strTable += frameBar + '\n'
      strTable += dataRow.format(*item) + '\n'
      if self._barBelow[i]:
        strTable += frameBar + '\n'
    strTable += frameBar + '\n'

    return strTable

  def nrows(self):
    return self._nrows

  def ncols(self):
    return self._ncols

#  def addRow(self, newRow, barBelow = False, barAbove = False):
#    # Check that the new row has correct number of elements
#    if len(newRow) != self.ncols():
#      raise AsciiTableRowLengthMismatchException(
#          'Expected `newRow` length {0}, but length was {1}'.format(
#            self.ncols(), len(newRow)))
#    # Check that the new row is all string data
#    for i in range(len(newRow)):
#      element = newRow[i]
#      if type(element) is not str:
#        raise AsciiTableNonStringException(
#            'Type of element {0} in `newRow` should be {1}, but was {2}'.format(
#              i, 'str', type(element)))
#    # Add row
#    self._data.append(newRow)
#    self._barBelow.append(barBelow)
#    self._barAbove.append(barAbove)
#    # Incremenet number of rows
#    self._nrows += 1
#    # Update column widths if necessary
#    self.updateMaxColumnWidths()

  def addRow(self, *args, **kwargs):
    
    # Parse whether we want bars above/below our row.
    barBelow = False
    barAbove = False

    for key, value in kwargs.items():
      if key == 'barBelow':
          barBelow = value
      if key == 'barAbove':
          barAbove = value

    newRow = []

    # Check that the new row has correct number of elements, truncate/fill with defaults if necessary
    if len(args) != self.ncols():
      # If the user gives more arguments than columns then truncate.
      if len(args) > self.ncols():
        for i in range(self.ncols()):
          newRow.append(args[i])
      # If the user gives less arguments than columns then fill.
      elif len(args) < self.ncols():
        for i in range(self.ncols()):
          if i < len(args):
            newRow.append(args[i])
          else:
            newRow.append(self._defaultBlank)
    elif len(args) == self.ncols():
      # If the user gives the same data as no. of colums, copy over.
      for v in args:
        newRow.append(v)

    # Check that the new row is all string data, if not do the correct conversion
    for i in range(len(newRow)):
      element = newRow[i]
      if type(element) is not str:
        if type(element) is float:
          newRow[i] = self._defaultFloatFormat.format(element)
        elif type(element) is int:
          newRow[i] = self._defaultIntFormat.format(element)
        else:
          raise AsciiTableNonStringException(
              'Type of element {0} should be {1}, but was {2}'.format(
               i, 'str', type(element)))

    # Add row
    self._data.append(newRow)
    self._barBelow.append(barBelow)
    self._barAbove.append(barAbove)

    # Incremenet number of rows
    self._nrows += 1
    # Update column widths if necessary
    self.updateMaxColumnWidths()

  def setHeadings(self, *args):
    # Check that the new headings has correct number of elements
    if len(args) != self.ncols():
      raise AsciiTableRowLengthMismatchException(
          'Expected `args` length {0}, but length was {1}'.format(
            self.ncols(), len(args)))
    # Check that the new heading is all string data
    for i in range(len(args)):
      element = args[i]
      if type(element) is not str:
        raise AsciiTableNonStringException(
            'Type of element {0} in `args` should be {1}, but was {2}'.format(
              i, 'str', type(element)))
    # Update headings
    self._headings = args
    # Update column widths if necessary
    self.updateMaxColumnWidths()

  def updateMaxColumnWidths(self, reset=False):
    # Reset to zero.
    self._maxColWidths = [0] * self._ncols

    # First use headings for widths.
    for i in range(self.ncols()):
      heading = self._headings[i]
      if self._maxColWidths[i] < len(heading):
        self._maxColWidths[i] = len(heading)

    # Check through each data item.
    for j in range(self.ncols()):
      for i in range(self.nrows()):
        data = self._data[i][j]
        if self._maxColWidths[j] < len(data):
          self._maxColWidths[j] = len(data)

class AsciiTableRowLengthMismatchException(Exception):
  def __init__(self, value):
    self.value = value
  def __str__(self):
    return repr(self.value)

class AsciiTableNonStringException(Exception):
  def __init__(self, value):
    self.value = value
  def __str__(self):
    return repr(self.value)

if __name__ == '__main__':
  # Create a table with four columns
  tbl1 = AsciiTable(4)

  # Give the table headings
  tbl1.setHeadings('one', 'two', 'three', 'four')

  # Add two rows of data (they should all be strings)
  tbl1.addRow('mumra', 'the', 'ever', 'living')
  tbl1.addRow('{0:20.15f}'.format(19.1919191), '{0:20.15f}'.format(20.11339), '{0:10.5f}'.format(12.11939399), '{0:20.15f}'.format(.9399))
  tbl1.addRow(19.1919191, 20.11339, 12.11939399, 0.9399)
  tbl1.addRow(12993.12342, 10, 'hello', 'world')
  tbl1.addRow('one', 'two', 'three', barAbove=True, barBelow=True)
  tbl1.addRow(129.142, 10, 20, 30)
  tbl1.addRow(129.142, 10, 20, 30, 40, 50, 60)

  print tbl1
