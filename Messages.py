class bcolours:
    HEADER    = '\033[95m'
    OKBLUE    = '\033[94m'
    OKGREEN   = '\033[92m'
    WARNING   = '\033[93m'
    FAIL      = '\033[91m'
    ENDC      = '\033[0m'
    BOLD      = '\033[1m'
    UNDERLINE = '\033[4m'

def warning(msg):
    print("{}[WARNING]{} {}".format(bcolours.OKBLUE, bcolours.ENDC, msg))

def info(msg):
    print("{}[   INFO]{} {}".format(bcolours.OKGREEN, bcolours.ENDC, msg))

def error(msg):
    print("{}[  ERROR]{} {}".format(bcolours.FAIL, bcolours.ENDC, msg))
