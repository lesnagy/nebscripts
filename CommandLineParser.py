import re

class CommandLineParseError:
    def __init__(self, value):
        self._value = value

    def __str__(self):
        return repr(self._value)


class MaterialCommandLineParser:
    def __init__(self):
        self.regexMaterial = re.compile(r'material=([a-zA-Z0-9]+)')

    def get(self, arg):
        match = self.regexMaterial.match(arg)
        if match:
            return match.group(1)
        else:
            return None

class GeometryCommandLineParser:
    def __init__(self):
        self.regexGeometry = re.compile(r'geometry=([a-zA-Z0-9]+)')

    def get(self, arg):
        match = self.regexGeometry.match(arg)
        if match:
            return match.group(1)
        else:
            return None

class SizeCommandLineParser:
    def __init__(self):
        self.regexSize = re.compile(r'size=([0-9]+)')

    def get(self, arg):
        match = self.regexSize.match(arg)
        if match:
            return int(match.group(1))
        else:
            return None

class TemperatureCommandLineParser:
    def __init__(self):
        self.regexTemperature = re.compile(r'temperature=([0-9]+)')

    def get(self, arg):
        match = self.regexTemperature.match(arg)
        if match:
            return int(match.group(1))
        else:
            return None

class IndexCommandLineParser:
    def __init__(self):
        self.regexIndex = re.compile(r'index=([a-zA-Z0-9]+)')

    def get(self, arg):
        match = self.regexIndex.match(arg)
        if match:
            return match.group(1)
        else:
            return None

class SizesCommandLineParser:
    def __init__(self):
        self.regexSizeRange = re.compile(r'sizes=([0-9]+):([0-9]+):([0-9]+)')
        self.regexSizeList  = re.compile(r'sizes=(([0-9]+)(,\s*[0-9]+)*)')

    def get(self, arg):
        matchSizeRange = self.regexSizeRange.match(arg)
        if matchSizeRange:
            start = int(matchSizeRange.group(1))
            end   = int(matchSizeRange.group(2))
            step  = int(matchSizeRange.group(3))
            return range(start, end+step, step)

        matchList = self.regexSizeList.match(arg)
        if matchList:
            return [ int(x) for x in re.split(r',\s*', matchList.group(1)) ]

        return None

class TemperaturesCommandLineParser:
    def __init__(self):
        self.regexSizeRange = re.compile(r'temperatures=([0-9]+):([0-9]+):([0-9]+)')
        self.regexSizeList  = re.compile(r'temperatures=(([0-9]+)(,\s*[0-9]+)*)')

    def get(self, arg):
        matchSizeRange = self.regexSizeRange.match(arg)
        if matchSizeRange:
            start = int(matchSizeRange.group(1))
            end   = int(matchSizeRange.group(2))
            step  = int(matchSizeRange.group(3))
            return range(start, end+step, step)

        matchList = self.regexSizeList.match(arg)
        if matchList:
            return [ int(x) for x in re.split(r',\s*', matchList.group(1)) ]

        return None

class IndicesCommandLineParser:
    def __init__(self):
        self.regexSizeRange = re.compile(r'indices=([0-9]+):([0-9]+):([0-9]+)')
        self.regexSizeList  = re.compile(r'indices=(([0-9]+)(,\s*[0-9]+)*)')

    def get(self, arg):
        matchSizeRange = self.regexSizeRange.match(arg)
        if matchSizeRange:
            start = int(matchSizeRange.group(1))
            end   = int(matchSizeRange.group(2))
            step  = int(matchSizeRange.group(3))
            return range(start, end+step, step)

        matchList = self.regexSizeList.match(arg)
        if matchList:
            return [ int(x) for x in re.split(r',\s*', matchList.group(1)) ]

        return None

class InputFileCommandLineParser:
    def __init__(self):
        self.regexInputFile = re.compile(r'infile=(.*)')

    def get(self, arg):
        matchInputFile = self.regexInputFile.match(arg)
        if matchInputFile:
            fileName = matchInputFile.group(1)
            return fileName

        return None

class OutputFileCommandLineParser:
    def __init__(self):
        self.regexOutputFile = re.compile(r'outfile=(.*)')

    def get(self, arg):
        matchOutputFile = self.regexOutputFile.match(arg)
        if matchOutputFile:
            fileName = matchOutputFile.group(1)
            return fileName

        return None

class PoolSizeCommandLineParser:
    def __init__(self):
        self.regexPoolSize = re.compile(r'poolsize=([0-9]+)')

    def get(self, arg):
        matchPoolSize = self.regexPoolSize.match(arg)
        if matchPoolSize:
            poolsize = int(matchPoolSize.group(1))
            return poolsize

class NumberOfStepsLineParser:
    def __init__(self):
        self.regexNoOfSteps = re.compile(r'nsteps=([0-9]+)')

    def get(self, arg):
        matchNoOfSteps = self.regexNoOfSteps.match(arg)
        if matchNoOfSteps:
            noOfSteps = int(matchNoOfSteps.group(1))
            return noOfSteps

class CommandLine:
    def __init__(self, *args):

        self._parsers = {}
        self._porder  = []
        self._values  = {}

        for arg in args:
            if arg == 'material':
                self._parsers[arg] = MaterialCommandLineParser()
                self._porder.append(arg)
            elif arg == 'geometry':
                self._parsers[arg] = GeometryCommandLineParser()
                self._porder.append(arg)
            elif arg == 'sizes':
                self._parsers[arg] = SizesCommandLineParser()
                self._porder.append(arg)
            elif arg == 'size':
                self._parsers[arg] = SizeCommandLineParser()
                self._porder.append(arg)
            elif arg == 'temperatures':
                self._parsers[arg] = TemperaturesCommandLineParser()
                self._porder.append(arg)
            elif arg == 'temperature':
                self._parsers[arg] = TemperatureCommandLineParser()
                self._porder.append(arg)
            elif arg == 'indices':
                self._parsers[arg] = IndicesCommandLineParser()
                self._porder.append(arg)
            elif arg == 'index':
                self._parsers[arg] = IndexCommandLineParser()
                self._porder.append(arg)
            elif arg == 'infile':
                self._parsers[arg] = InputFileCommandLineParser()
                self._porder.append(arg)
            elif arg == 'outfile':
                self._parsers[arg] = OutputFileCommandLineParser()
                self._porder.append(arg)
            elif arg == 'poolsize':
                self._parsers[arg] = PoolSizeCommandLineParser()
                self._porder.append(arg)
            elif arg == 'nsteps':
                self._parsers[arg] = NumberOfStepsLineParser()
                self._porder.append(arg)
            else:
                pass

    def parseArgv(self, argv):
        self._values.clear()

        for arg in argv:
            for pname in self._porder:
                parser = self._parsers[pname]
                parseValue = parser.get(arg)
                if parseValue != None:
                    self._values[pname] = parseValue
                    break

        # Validate our values
        for pname in self._porder:
            if not pname in self._values.keys():
                raise CommandLineParseError("The argument '{}' has no value".format(pname))

    def getArg(self, argName):
        if argName in self._values.keys():
            return self._values[argName]
        return None

